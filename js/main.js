document.getElementById("hello_text").textContent="はじめてのJavaScript";
document.addEventListener("keyup",onKeyUp);


var count=0;
var cells;
var rowNUM=20;
var colNUM=10;


//ブロックのパターン
var blocks={
  i:{
    class:"i",
    pattern:[
      [1,1,1,1]
    ]
  },
  o:{
    class:"o",
    pattern:[
      [1,1],
      [1,1]
    ]
  },
  t: {
    class: "t",
    pattern: [
      [0, 1, 0],
      [1, 1, 1]
    ]
  },
  s: {
    class: "s",
    pattern: [
      [0, 1, 1],
      [1, 1, 0]
    ]
  },
  z: {
    class: "z",
    pattern: [
      [1, 1, 0],
      [0, 1, 1]
    ]
  },
  j: {
    class: "j",
    pattern: [
      [1, 0, 0],
      [1, 1, 1]
    ]
  },
  l: {
    class: "l",
    pattern: [
      [0, 0, 1],
      [1, 1, 1]
    ]
  }
};

loadTable();
setInterval(function () {
  count++;
  document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";
  // ブロックが積み上がり切っていないかのチェック
  for (var row = 0; row < 2; row++) {
    for (var col = 0; col < 10; col++) {
      if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
        alert("game over");
        init();
      }
    }
  }
  if (hasFallingBlock()) { // 落下中のブロックがあるか確認する
    fallBlocks();// あればブロックを落とす
  } else { // なければ
    deleteRow();// そろっている行を消す
    generateBlock();// ランダムにブロックを作成する
  }
}, 250);

function loadTable(){
  cells=[];
  var td_array = document.getElementsByTagName("td");
  var index = 0;
  for (var row = 0; row < rowNUM; row++) {
    cells[row] = [];
    for (var col = 0; col < colNUM; col++) {
      cells[row][col] = td_array[index];
      index++;
    }
  }
}


function fallBlocks() {
  // 1. 底についていないか？
  for (var col = 0; col < colNUM; col++) {
    if (cells[rowNUM-1][col].blockNum === fallingBlockNum) {
      isFalling = false;
      return; // 一番下の行にブロックがいるので落とさない
    }
  }
  // 2. 1マス下に別のブロックがないか？
  for (var row = rowNUM-2; row >= 0; row--) {
    for (var col = 0; col < colNUM; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
          isFalling = false;
          return; // 一つ下のマスにブロックがいるので落とさない
        }
      }
    }
  }
  // 下から二番目の行から繰り返しクラスを下げていく
  for (var row = rowNUM-2; row >= 0; row--) {
    for (var col = 0; col < colNUM; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

var isFalling = false;
function hasFallingBlock() {
  // 落下中のブロックがあるか確認する
  return isFalling;
}
function deleteRow() {
  // そろっている行を消す
  for (var row = rowNUM-1; row >= 0; row--) {
    var canDelete = true;
    for (var col = 0; col < colNUM; col++) {
      if (cells[row][col].className === "") {
        canDelete = false;
      }
    }
    if (canDelete) {
      // 1行消す
      for (var col = 0; col < colNUM; col++) {
        cells[row][col].className = "";
      }
      // 上の行のブロックをすべて1マス落とす
      for (var downRow = row - 1; downRow >= 0; downRow--) {
        for (var col = 0; col < colNUM; col++) {
          cells[downRow + 1][col].className = cells[downRow][col].className;
          cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
          cells[downRow][col].className = "";
          cells[downRow][col].blockNum = null;
        }
      }
    }
  }
}
var fallingBlockNum = 0;
function generateBlock() {
  // ランダムにブロックを生成する
  // 1. ブロックパターンからランダムに一つパターンを選ぶ
  var keys = Object.keys(blocks);
  var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
  var nextBlock = blocks[nextBlockKey];
  var nextFallingBlockNum = fallingBlockNum + 1;
  // 2. 選んだパターンをもとにブロックを配置する
  var pattern = nextBlock.pattern;
  for (var row = 0; row < pattern.length; row++) {
    for (var col = 0; col < pattern[row].length; col++) {
      if (pattern[row][col]) {
        cells[row][col + 3].className = nextBlock.class;
        cells[row][col + 3].blockNum = nextFallingBlockNum;
      }
    }
  }
  // 3. 落下中のブロックがあるとする
  isFalling = true;
  fallingBlockNum = nextFallingBlockNum;
}

function moveRight() {
  // ブロックを右に移動させる
  var first=true;
  for (var row = 0; row < rowNUM; row++) {
    for (var col = 9; col >= 0; col--) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if(first){
          switch(cells[row][col].className){
            case "t":
            if(col===colNUM-2 || cells[row+1][col+2].className!==""){
              return;
            }
            case "z":
              if(col===colNUM-2){
                return;
              }
            case "j":
              if(col===colNUM-3){
                return;
              }
            default:
              if(col===colNUM-1){
                return;
              }
          }
          first=false;
        }
        cells[row][col + 1].className = cells[row][col].className;
        cells[row][col + 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}
function moveLeft() {
  // ブロックを左に移動させる
  for (var row = 0; row < rowNUM; row++) {
    for (var col = 0; col < colNUM; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if(col===0){
          return;
        }
        else{
          cells[row][col - 1].className = cells[row][col].className;
          cells[row][col - 1].blockNum = cells[row][col].blockNum;
          cells[row][col].className = "";
          cells[row][col].blockNum = null;
        }
      }
    }
  }
}

function moveDown(){
  for (var row = rowNUM-2; row >= 0; row--) {
    for (var col = 0; col < colNUM; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
          isFalling = false;
          return; // 一つ下のマスにブロックがいるので落とさない
        }
        cells[row+1][col].className = cells[row][col].className;
        cells[row+1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

function init(){
  cells=[];
  for (var row = 0; row < rowNUM; row++) {
    cells[row] = [];
    for (var col = 0; col < colNUM; col++) {
      cells[row][col].className = "";
      cells[row][col].blockNum = null;
    }
  }
  count=0;
  fallingBlockNum=0
}


function onKeyUp(event){

  if(event.keyCode===37){
    moveLeft();
  }
  else if(event.keyCode===39){
    moveRight();
  }
  else if(event.keyCode===40){
    moveDown();
  }

}
